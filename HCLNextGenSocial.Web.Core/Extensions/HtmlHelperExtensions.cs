﻿using System.Web.Mvc;
using HCLNextGenSocial.Web.Core.Helpers;

namespace HCLNextGenSocial.Web.Core.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static UserHtmlHelper User(this HtmlHelper html)
        {
            return new UserHtmlHelper(html, new UrlHelper(html.ViewContext.RequestContext));
        }
    }
}