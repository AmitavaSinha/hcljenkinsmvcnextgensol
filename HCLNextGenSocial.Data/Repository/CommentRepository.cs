﻿using HCLNextGenSocial.Data.Infrastructure;
using HCLNextGenSocial.Model.Models;
namespace HCLNextGenSocial.Data.Repository
{
    public class CommentRepository: RepositoryBase<Comment>, ICommentRepository
        {
        public CommentRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
            {
            }
      
        }
    public interface ICommentRepository : IRepository<Comment>
    {    

    }
}