﻿using HCLNextGenSocial.Data.Infrastructure;
using HCLNextGenSocial.Model.Models;
namespace HCLNextGenSocial.Data.Repository
{
    public class FocusRepository: RepositoryBase<Focus>, IFocusRepository
        {
        public FocusRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
            {
            }           
        }
    public interface IFocusRepository : IRepository<Focus>
    {
    }
}