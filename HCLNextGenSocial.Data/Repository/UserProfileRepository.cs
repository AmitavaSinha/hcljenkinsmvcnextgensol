﻿using HCLNextGenSocial.Data.Infrastructure;
using HCLNextGenSocial.Model.Models;

namespace HCLNextGenSocial.Data.Repository
{
    public class UserProfileRepository : RepositoryBase<UserProfile>, IUserProfileRepository
    {
        public UserProfileRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface IUserProfileRepository : IRepository<UserProfile>
    {
    }
}
