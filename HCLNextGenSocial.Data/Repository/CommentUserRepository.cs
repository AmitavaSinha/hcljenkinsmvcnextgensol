﻿using HCLNextGenSocial.Model.Models;
using HCLNextGenSocial.Data.Infrastructure;

namespace HCLNextGenSocial.Data.Repository
{
    public class CommentUserRepository : RepositoryBase<CommentUser>, ICommentUserRepository
    {
        public CommentUserRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
        
    }
    public interface ICommentUserRepository : IRepository<CommentUser>
    {
        
    }
}
