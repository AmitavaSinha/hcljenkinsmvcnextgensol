﻿using HCLNextGenSocial.Data.Infrastructure;
using HCLNextGenSocial.Data.Models;
using HCLNextGenSocial.Model.Models;
using System;
using System.Linq.Expressions;
namespace HCLNextGenSocial.Data.Repository
{
    public class UserRepository: RepositoryBase<ApplicationUser>, IUserRepository
        {
        public UserRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
            {
            }        
        }
    public interface IUserRepository : IRepository<ApplicationUser>
    {
        
    }
}