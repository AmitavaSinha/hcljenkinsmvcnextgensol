﻿using HCLNextGenSocial.Data.Infrastructure;
using HCLNextGenSocial.Model.Models;
namespace HCLNextGenSocial.Data.Repository
{
    public class GroupCommentRepository : RepositoryBase<GroupComment>, IGroupCommentRepository
    {
        public GroupCommentRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface IGroupCommentRepository : IRepository<GroupComment>
    {
    }
}