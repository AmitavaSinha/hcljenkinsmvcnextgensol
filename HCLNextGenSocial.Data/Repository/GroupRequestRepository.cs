﻿using HCLNextGenSocial.Data.Infrastructure;
using HCLNextGenSocial.Model.Models;
namespace HCLNextGenSocial.Data.Repository
{
    public class GroupRequestRepository: RepositoryBase<GroupRequest>, IGroupRequestRepository
        {
        public GroupRequestRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
            {
            }           
        }
    public interface IGroupRequestRepository : IRepository<GroupRequest>
    {
    }
}