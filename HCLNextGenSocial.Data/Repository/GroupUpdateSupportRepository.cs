﻿using System;
using HCLNextGenSocial.Model.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HCLNextGenSocial.Data.Infrastructure;

namespace HCLNextGenSocial.Data.Repository
{
    class GroupUpdateSupportRepository : RepositoryBase<GroupUpdateSupport>, IGroupUpdateSupportRepository
    {
        public GroupUpdateSupportRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface IGroupUpdateSupportRepository : IRepository<GroupUpdateSupport>
    {
    }
}
