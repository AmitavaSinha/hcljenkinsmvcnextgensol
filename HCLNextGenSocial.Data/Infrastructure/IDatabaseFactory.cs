﻿using System;
using HCLNextGenSocial.Data.Models;

namespace HCLNextGenSocial.Data.Infrastructure
{
    public interface IDatabaseFactory : IDisposable
    {
        SocialGoalEntities Get();
    }
}
