﻿
namespace HCLNextGenSocial.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
