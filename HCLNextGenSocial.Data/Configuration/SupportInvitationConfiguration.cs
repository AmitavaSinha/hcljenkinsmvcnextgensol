﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using HCLNextGenSocial.Model.Models;

namespace HCLNextGenSocial.Data.Configuration
{
    public class SupportInvitationConfiguration:EntityTypeConfiguration<SupportInvitation>
    {
        public SupportInvitationConfiguration()
        {
            Property(s => s.GoalId).IsRequired();
            Property(s => s.SentDate).IsRequired();
            Property(s => s.Accepted).IsRequired();
            Property(s => s.FromUserId).IsMaxLength();
            Property(s => s.ToUserId).IsMaxLength();
        }
    }
}
