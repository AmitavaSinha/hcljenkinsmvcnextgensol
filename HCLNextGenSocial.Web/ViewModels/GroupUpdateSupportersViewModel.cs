﻿using System.Collections.Generic;
using HCLNextGenSocial.Model.Models;

namespace HCLNextGenSocial.Web.ViewModels
{
    public class GroupUpdateSupportersViewModel
    {
        public int GroupUpdateId { get; set; }

        public GroupUpdate GroupUpdate { get; set; }

        public IEnumerable<ApplicationUser> Users { get; set; }
    }
}