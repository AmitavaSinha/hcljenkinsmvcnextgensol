﻿using System.Collections.Generic;
using HCLNextGenSocial.Model.Models;

namespace HCLNextGenSocial.Web.ViewModels
{
    public class UpdateListViewModel
    {
        public IEnumerable<UpdateViewModel> Updates { get; set; }

        public double? Target { get; set; }

        public Metric Metric { get; set; }

    }
}