﻿using PagedList;
using HCLNextGenSocial.Service;

namespace HCLNextGenSocial.Web.ViewModels
{
    public class GroupsPageViewModel
    {
        public IPagedList<GroupsItemViewModel> GroupList { get; set; }

        public GroupFilter Filter { get; set; }
    }
}