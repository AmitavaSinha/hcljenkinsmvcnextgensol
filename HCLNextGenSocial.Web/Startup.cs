﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HCLNextGenSocial.Startup))]
namespace HCLNextGenSocial
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
